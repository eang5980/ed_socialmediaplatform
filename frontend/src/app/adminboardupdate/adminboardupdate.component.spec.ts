import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminboardupdateComponent } from './adminboardupdate.component';

describe('AdminboardupdateComponent', () => {
  let component: AdminboardupdateComponent;
  let fixture: ComponentFixture<AdminboardupdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminboardupdateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminboardupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
