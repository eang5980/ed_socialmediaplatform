import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../entity/user';
import { AdminboardService } from '../_services/adminboard.service';

@Component({
  selector: 'app-adminboardupdate',
  templateUrl: './adminboardupdate.component.html',
  styleUrls: ['./adminboardupdate.component.css']
})
export class AdminboardupdateComponent implements OnInit {

id!:number;
 user: User = new User();
  constructor(private adminboardService: AdminboardService,
    private route:ActivatedRoute, private router:Router) { }

  ngOnInit(): void {
    this.id=this.route.snapshot.params['id'];
    this.adminboardService.getUserbyId(this.id).subscribe(data=>{
      this.user=data;
    },error=>console.log(error))
  }

  onUpdateSubmit(){
    this.adminboardService.updateUser(this.id,this.user).subscribe({
      error:data=>{
      if(data.status ==200){
        alert("User updated successfully!");
      this.router.navigate(['/home/adminboard'])
      } else{
        console.log(data)
      }
    }
  });
}



}

