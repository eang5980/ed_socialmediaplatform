import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../entity/user'
import { AdminboardService } from '../_services/adminboard.service';
import { StorageService } from '../_services/storage.service';

@Component({
  selector: 'app-adminboard',
  templateUrl: './adminboard.component.html',
  styleUrls: ['./adminboard.component.css']
})
export class AdminboardComponent implements OnInit {

  users!: User[];
  roles: string[] = [];
  rolename!: string;

  currentemail!:string;

  createdDate!:string;
  lastModifiedDate!: Date;

  newDate!:Date;
 
  constructor(private adminboardService: AdminboardService, private router:Router,private storageService: StorageService) { }

  ngOnInit(): void {
    this.currentemail = this.storageService.getUser().email;
    this.getUsers();
  }

  // let dt = new Date();
  // dt.setHours(dt.getHours() + 2);

  private getUsers(){
    
    this.adminboardService.getUsersList()

    .subscribe(data =>{
    
      this.users =  data.map((d: any) => {
        this.roles=[];
        d.createdDate=(Date.parse(d.createdDate)  + 28800000)
        d.createdDate=  new Date(d.createdDate).toISOString();

        d.lastModifiedDate = (Date.parse(d.lastModifiedDate)  + 28800000)
        d.lastModifiedDate = new Date(d.lastModifiedDate).toISOString();
  
        for(var i of d.roles){
          this.rolename= JSON.stringify(i.name);
          this.rolename = this.rolename.substring(6,this.rolename.length-1);
          this.roles.push(this.rolename);
          this.rolename="";
        }
        let user : User =  {
          
          name: d.name,
          email: d.email,
          id: d.id,
          roles: this.roles,
          createdDate: d.createdDate.replace('.000Z', '').replace('T',' '),
          lastModifiedDate: d.lastModifiedDate.replace('.000Z', '').replace('T',' '),
          lastModifiedBy: d.lastModifiedBy,
          
        };
        return user;
      });
    });
  }

  addHoursToDate(date: Date, hours: number): Date {
    return new Date(new Date(date).setHours(date.getHours() + hours));
  }

  updateUser(id:number){
    this.router.navigate(['home/adminboard/updateuser',id]);
  }

  deleteUserbtn(id:number){
    if (confirm("Are you sure? Deleting is irreversible! ")) {
      this.adminboardService.deleteUser(id).subscribe({
        error:data=>{
        if(data.status ==200){
          alert("User Deleted!");
          this.getUsers();
        } else{
          console.log(data)
        }
      }
    });
    }
    
  }

}
