import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Post } from '../entity/post';
import { DisplaypostComponent } from '../displaypost/displaypost.component';
import { PostService } from '../_services/post.service';
import { StorageService } from '../_services/storage.service';
import { Observable } from 'rxjs';




interface createpostType {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-createpost',
  templateUrl: './createpost.component.html',
  styleUrls: ['./createpost.component.css']

})


export class CreatepostComponent implements OnInit {

  post: Post = new Post();


  @ViewChild('fileInput', { static: false })
  FileInput!: ElementRef;
  reactionform!: FormGroup;
  //createpostTypeval!: String;
  fileupload!: File;

  postId!: Number;

  fileuploadId!: String;

  isLink: boolean = false;
  format!: string;
  url!: string | ArrayBuffer | null;
  submitpostFlag: boolean = false;




  constructor(private postService: PostService, private formbuilder: FormBuilder, private dialog: MatDialog, private storageService: StorageService) { }

  ngOnInit(): void {
    this.post.caption=""
    this.post.link=""
  }


  createpostTypeval = 'link';

  createpostType: createpostType[] = [
    { value: 'link', viewValue: 'Link' },
    { value: 'media', viewValue: 'Media' },
  ];
  setisLink(val: boolean) {
    this.isLink = val;

  }
  isValidUrl = (urlString: string) => {
    var urlPattern = new RegExp('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?');
    return !!urlPattern.test(urlString);
    
  }

  createpostdismissBtn() {
    if (confirm("Closing this will discard this post! Are you sure?")) {
      this.dialog.closeAll();
    }
    this.resetFile();
  }
  onFilechange(event: any) {
    console.log(event.target.files[0])
    this.fileupload = event.target.files[0]

    if (this.fileupload) {
      var reader = new FileReader();
      reader.readAsDataURL(this.fileupload);
      if (this.fileupload.type.indexOf('image') > -1) {
        this.format = 'image';
      } else if (this.fileupload.type.indexOf('video') > -1) {
        this.format = 'video';
      }
      reader.onload = (event) => {
        this.url = (<FileReader>event.target).result;
      }
    }

  }
  resetFile() {
    this.fileupload = new File([], '');
  }
  resetPostlink() {
    this.post.link = "";
  }
  createpostMediaSubmitBtn(inputs: any) {

    if (this.post.caption == null || this.post.caption=="" ) {
      alert("Caption cannot be empty!")
    } else {
      if(this.format != 'video' && this.format !='image')  {
        alert("Ensure file uploaded is an image or video!")
        this.resetFile();
        return
      }

      
      this.resetPostlink();
      this.post.contenttype = 'media';

      this.postService.uploadfile(this.fileupload).subscribe({
        next: data => {
          this.fileuploadId = JSON.stringify(data.fileId).slice(1, -1);


          this.postService.createpost(this.post, this.storageService.getUser().email).subscribe({
            next: data => {

              this.postId = Number(JSON.stringify(data.id));
              this.postService.updatefile(this.fileuploadId, this.postId).subscribe({
                error: data => {
                  if (data.status == 200) {
                    alert("Create Post Successfully");
                    this.reloadPage();
                    this.resetFile();
                    this.dialog.closeAll();
                    
                    
                  } else {
                    alert("Create Post error.")
                  }
                }
              });
            },
            error: data => {
              alert("Create post error.")
            }
          });
        },
        error: err => {
          alert("Error uploading file. Ensure file field is not empy.")
        }
      })
      
      this.resetFile();
     
    }

  }






  createpostLinkSubmitBtn(inputs: any) {
    this.post.link = this.post.link.trim();
    if ( (this.post.caption == null || this.post.caption.trim()=="") || (this.post.link==null || this.post.link.trim()=="")  )  {
      
      if( (this.post.caption == null || this.post.caption.trim()=="")){
      alert("Caption cannot be empty!")
      }
      if ( (this.post.link==null || this.post.link.trim()=="") ){
        alert("Link cannot be empty!")
      }

    } else {

      if (this.isValidUrl(this.post.link)) {

        this.resetFile();
        this.post.contenttype = 'link';
        this.postService.createpost(this.post, this.storageService.getUser().email).subscribe({
          next: data => {
            alert("Post created successfully!");
            this.reloadPage();
            this.resetPostlink();
            this.dialog.closeAll();
          },
          error: data => {
            alert("create post error.")
          }
        });
        this.resetPostlink();
        
      } else {
        alert("Please enter a valid URL")
      }
    }

   
  }

  reloadPage(): void {
    window.location = window.location
  }
}



