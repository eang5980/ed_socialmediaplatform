import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreatepostComponent } from '../createpost/createpost.component';



@Component({
  selector: 'app-postfeed',
  templateUrl: './postfeed.component.html',
  styleUrls: ['./postfeed.component.css']
})
export class PostfeedComponent implements OnInit {

  constructor(private dialog:MatDialog) { }

  ngOnInit(): void {
   
  }
  onCreatePostClick(){
    this.dialog.open(CreatepostComponent,{ disableClose: true })
  }

 

}
