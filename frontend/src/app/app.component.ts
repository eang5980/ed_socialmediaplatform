import { Component } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { AuthenticatorComponent } from './authenticator/authenticator.component';
import { AuthService } from './_services/auth.service';
import { StorageService } from './_services/storage.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  private roles: string[] = [];
  isLoggedIn = false;

  showAdminBoard = false;
  showPostfeed=false;

  name?: string;

  constructor(private loginSheet: MatBottomSheet, private storageService: StorageService, private authService: AuthService){

  }


  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.name = user.name;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showPostfeed = this.roles.includes('ROLE_USER');
      
    }
  }













  public static getStartedorLogin:any;
  onLoginClick(){
    AppComponent.getStartedorLogin= 'login'
    this.loginSheet.open(AuthenticatorComponent);
  }
  onLogoutnavClick(){
    this.logout();
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: res => {
        console.log(res);
        this.storageService.clean();

        window.location.reload();
      },
      error: err => {
        console.log(err);
      }
    });
  }
}
