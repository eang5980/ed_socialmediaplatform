import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Post } from '../entity/post';
import { PostService } from '../_services/post.service';
import { DisplaypostComponent } from '../displaypost/displaypost.component';


@Component({
  selector: 'app-editpost',
  templateUrl: './editpost.component.html',
  styleUrls: ['./editpost.component.css']
})
export class EditpostComponent implements OnInit {

  constructor(private postService: PostService, private displaypostComponent: DisplaypostComponent, @Inject(MAT_DIALOG_DATA) public data: string,
  private dialogRef: MatDialogRef<EditpostComponent>) { }
  post: Post = new Post();



  ngOnInit(): void {

  }
  dismissBtn() {
    if (confirm("Closing this will discard current edits! Are you sure?")) {
      this.dialogRef.close( {data: '' })
      //this.dialog.closeAll();
    }
  }
  editSubmitBtn() {
    if (this.post.caption.trim() == "") {
      alert("Caption cannot be empty!")
    } else {

      this.postService.updatepost(this.post.id, this.post).subscribe((data: any) => {
        alert("Updated Successfully!")

        this.dialogRef.close({ data: this.post.caption })


        //  this.displaypostComponent.getAllData();



      })


    }
  }

}
