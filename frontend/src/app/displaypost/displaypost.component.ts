import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { PostService } from '../_services/post.service';
import { AppComponent } from '../app.component';
import { Post } from '../entity/post';
import { StorageService } from '../_services/storage.service';
import { MatDialog } from '@angular/material/dialog';
import { EditpostComponent } from '../editpost/editpost.component';
import { ShowvideoComponent } from '../showvideo/showvideo.component';
import { ShowimageComponent } from '../showimage/showimage.component';



@Component({
  selector: 'app-displaypost',
  templateUrl: './displaypost.component.html',
  styleUrls: ['./displaypost.component.css']
})
export class DisplaypostComponent implements OnInit {

  
  constructor(private postService: PostService, private appComponent: AppComponent, private storageService: StorageService, private dialog: MatDialog) {


  }

  page: number = 1;
  itemsPerPage = 10;
  postView!: number;
  post: Post = new Post();

  currentUser = this.storageService.getUser().name;

  currentRoles = this.storageService.getUser().roles;
 
  parseDate(date:any,post:Post){
    date=(Date.parse(date)  + 28800000)
    date=  new Date(date).toISOString();
    date=date.replace('.000Z', '').replace('T',' ')
    return date
  }

  isAdmin() {
    if (this.currentRoles.includes('ROLE_ADMIN')) {
      return true;
    } else {
      return false;
    }
  }

  ngOnInit(): void {
    this.getAllData()
  }

  posts: any;



  getAllData() {
    this.postService.getAllpost().subscribe((data: any) => {
     
      this.posts = data.content;

    })
  }

  editpostBtn(postid: number, caption: string, post: Post) {

    let editpostBtndialogRef = this.dialog.open(EditpostComponent, { disableClose: true })
    editpostBtndialogRef.componentInstance.post.caption = caption
    editpostBtndialogRef.componentInstance.post.id = postid

    editpostBtndialogRef.afterClosed().subscribe(res => {

      if (res.data == '') {

      } else {
        post.caption = res.data
      }

    })

  }

  updatePostcaption(updateCap: string) {
    this.post.caption = updateCap;
  }

  deletepost(postid: number) {
    if (confirm("Are you sure you want to delete? Deleting it is irreversible!") == true) {
      this.postService.deletepost(postid).subscribe((data: any) => {

        this.getAllData();
        alert("Delete Successfully! ")


      })
    } else {

    }
  }

  clickMediaPost(postid: number, post: Post, fileId: String) {
    let clickMediaPostdialogRef = this.dialog.open(ShowvideoComponent)
    clickMediaPostdialogRef.componentInstance.fileId = fileId;
    post.viewcount += 1;
    this.postService.updatepost(postid, post).subscribe()

  }

  clickImagePost(postid: number, post: Post, fileId: String) {
    let clickImagePostdialogRef = this.dialog.open(ShowimageComponent)
    clickImagePostdialogRef.componentInstance.fileId = fileId;
    post.viewcount += 1;
    this.postService.updatepost(postid, post).subscribe()
  }

  clickLinkPost(postid: number, post: Post) {
    post.viewcount += 1;
    this.postService.updatepost(postid, post).subscribe()
  }


}

