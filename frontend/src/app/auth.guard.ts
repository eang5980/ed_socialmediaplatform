import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from './_services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private storageService: StorageService, private router: Router) { }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let url: string = state.url;
    return this.checkUserLogin(route, url);
  }

  checkUserLogin(route: ActivatedRouteSnapshot, url: any): boolean {
    if (this.storageService.isLoggedIn()) {
      const userRole = this.storageService.getUser().roles;
      if (route.data['role'].some((r: any) => userRole.includes(r) == false && userRole.some((r: any) => route.data['role'].includes(r) == false))) {

        this.router.navigate(['/home/postfeed'])

        return false;

      }

      return true;
    }
    else {
      this.router.navigate(['/home/'])

      return false;

    }
  }

}
