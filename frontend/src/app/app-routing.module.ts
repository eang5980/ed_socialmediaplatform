import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminboardComponent } from './adminboard/adminboard.component';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from './home/home.component';
import { AdminboardupdateComponent } from './adminboardupdate/adminboardupdate.component';
import { PostfeedComponent } from './postfeed/postfeed.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent 
},
  { 
    path: 'home/postfeed', component:PostfeedComponent,
    canActivate: [AuthGuard],
    data:{
      role:['ROLE_ADMIN','ROLE_USER']
    }
  },
  { 
    path: 'home/adminboard', component:AdminboardComponent,
    canActivate: [AuthGuard],
    data:{
      role:['ROLE_ADMIN']
    }
  }, 
   
  { 
    path: 'home/adminboard/updateuser/:id', component:AdminboardupdateComponent,
    canActivate: [AuthGuard],
    data:{
      role:['ROLE_ADMIN']
    }
  },
  {
    path: "**",
    component: HomeComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
