export class User{
    id!: number;
    name!: string;
    email!: string;
    roles: string[] = [];
    createdDate!: Date;
    lastModifiedDate!: Date;
    lastModifiedBy!: String;
    

}