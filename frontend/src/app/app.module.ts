import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';


import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatCardModule } from '@angular/material/card';
import { AuthenticatorComponent } from './authenticator/authenticator.component';
import { HomeComponent } from './home/home.component';
import { FormsModule }   from '@angular/forms';
import { httpInterceptorProviders } from './_helper/auth.interceptor';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AlertModule, AlertConfig } from 'ngx-bootstrap/alert';
import { AdminboardComponent } from './adminboard/adminboard.component';
import { AdminboardupdateComponent } from './adminboardupdate/adminboardupdate.component';

import {MatIconModule} from '@angular/material/icon';
import { PostfeedComponent } from './postfeed/postfeed.component';
import { CreatepostComponent } from './createpost/createpost.component';

import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import { DisplaypostComponent } from './displaypost/displaypost.component';

import {NgxPaginationModule} from 'ngx-pagination';
import { EditpostComponent } from './editpost/editpost.component';
import {NgxLinkPreviewModule} from 'ngx-link-preview';
import { ShowvideoComponent } from './showvideo/showvideo.component';
import { ShowimageComponent } from './showimage/showimage.component';



@NgModule({
  declarations: [
    AppComponent,
    AuthenticatorComponent,
    HomeComponent,
    AdminboardComponent,
    AdminboardupdateComponent,
    PostfeedComponent,
    CreatepostComponent,
    DisplaypostComponent,
    EditpostComponent,
    ShowvideoComponent,
    ShowimageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatBottomSheetModule,
    MatCardModule,
    FormsModule,
    HttpClientModule,
    AlertModule, 
    ReactiveFormsModule,
    MatIconModule,
    MatDialogModule,
    MatSelectModule,
    MatInputModule,
    NgxPaginationModule,
    NgxLinkPreviewModule
    
   
  ],
  providers: [httpInterceptorProviders, AlertConfig,DisplaypostComponent,AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
