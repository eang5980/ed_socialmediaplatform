import { TestBed } from '@angular/core/testing';

import { AdminboardService } from './adminboard.service';

describe('AdminboardService', () => {
  let service: AdminboardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminboardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
