import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'http://localhost:8080/api/users/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    return this.http.post(
      AUTH_API + 'signin',
      {
        email,
        password,
      },
      httpOptions
    );
  }

  register(name: string, email: string, password: string): Observable<any> {
    return this.http.post(
      AUTH_API + 'signup',
      {
        name,
        email,
        password,
      },
      httpOptions
    );
  }

  forgotpassword(email: string): Observable<any> {
    let params = new HttpParams().set("email", email)
    return this.http.post(
      AUTH_API + 'forgotpassword?' + params,
      httpOptions
    );
  }

  resetpassword(token: string, newpassword:string): Observable<any> {
    let params = new HttpParams()
    .set("token", token)
    .set("password", newpassword);
    return this.http.put(
      AUTH_API + 'resetpassword?' + params,
      httpOptions
    );
  }

  logout(): Observable<any> {
    return this.http.post(AUTH_API + 'signout', { }, httpOptions);
  }
}