import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../entity/user';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root',
})
export class AdminboardService {
  private BASE_URL = 'http://localhost:8080/api/users/';
  constructor(private httpClient: HttpClient) { }

  getUsersList(): Observable<User[]>{
    return this.httpClient.get<User[]>(`${this.BASE_URL + 'getallusers'}`);
  }

  getUserbyId(id:number):Observable<User>{
    return this.httpClient.get<User>(`${this.BASE_URL}+ ${id}`);
  }

  updateUser(id:number, user: User): Observable<Object>{
    return this.httpClient.put(`${this.BASE_URL+ 'update/'}${id}`,user);
  }

  deleteUser(id:number): Observable<Object>{
    return this.httpClient.delete(`${this.BASE_URL}+${id}`);
  }

}
