import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../entity/post';

const BASE_API = 'http://localhost:8080/api/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  uploadfile(file: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post(
      BASE_API + 'files/uploadfile?',
      formData
    );
  }
  getFile(id:String):Observable<any> {
    return this.http.get(`${BASE_API+'files/downloadfile/'}${id}`);
  }


  updatefile(id:String,postid:Number):Observable<any>{
    return this.http.put(`${BASE_API+ 'files/'}${id}/${postid}`,null);
  }
  createpost(post: Post, email: String): Observable<any>{
    return this.http.post(`${BASE_API+ 'posts/'}${email}`,post);
  }
  updatepost(id:number, post: Post): Observable<any>{
    return this.http.put(`${BASE_API+ 'posts/'}${id}`,post);
  }
  deletepost(id:number): Observable<any>{
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.http.delete(`${BASE_API+ 'posts/'}${id}`, { headers, responseType: 'text'});
  }
  getAllpost():Observable<any> {
    return this.http.get(BASE_API+"posts/");
  }

}
