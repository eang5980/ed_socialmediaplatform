import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { StorageService } from '../_services/storage.service';
import { AppComponent } from '../app.component';
import { Router } from '@angular/router';
import { MatBottomSheet } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-authenticator',
  templateUrl: './authenticator.component.html',
  styleUrls: ['./authenticator.component.css']
})
export class AuthenticatorComponent implements OnInit {

  reactionform!: FormGroup;

  /** Reg fields */
  regname: any;
  regemail: any;
  regpassword: any;
  regconpassword: any;

  /** Login fields */
  loginemail: any;
  loginpassword: any;

  /** Forgotpwd field */
  forgotresetpwdemail: any;
  storeemail: any;


  /** Resetpwd field */
  resetpwdtoken: any;
  resetpwdnewpwd: any;



  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  isLoggedIn = false;
  isLoginFailed = false;
  roles: string[] = [];

  forgotresetpwdemailSubmit = false;

  resetpwdtokenSubmit = false;
  isresetFail = false;




  state!: AuthenticatorCompState;

  getStartedorLogin = AppComponent.getStartedorLogin



  constructor(private authService: AuthService, private formbuilder: FormBuilder, private storageService: StorageService, private router: Router, private loginSheet: MatBottomSheet) {

    if (this.getStartedorLogin == 'login') {
      this.state = AuthenticatorCompState.LOGIN
    } else if (this.getStartedorLogin == 'getstarted') {
      this.state = AuthenticatorCompState.REGISTER
    }

    if (this.state == AuthenticatorCompState.LOGIN) {
      this.onLoginClick();
    } else if (this.state == AuthenticatorCompState.REGISTER) {
      this.onCreateAccountClick();
    } else if (this.state == AuthenticatorCompState.FORGOT_PASSWORD) {
      this.onForgotPasswordClick();
    } else if (this.state == AuthenticatorCompState.RESET_PASSWORD) {
      this.onResetPasswordClick();
    }

  }

  ngOnInit(): void {
  }

  onResetPasswordClick() {
    this.state = AuthenticatorCompState.RESET_PASSWORD;

    this.reactionform = this.formbuilder.group({
      resetpwdtoken: new FormControl('', Validators.compose([Validators.required])),
      resetpwdnewpwd: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
      resetpwdconnewpwd: new FormControl('', Validators.compose([Validators.required])),
    },
      {
        validators: this.Pwdmustmatch('resetpwdnewpwd', 'resetpwdconnewpwd')
      }
    )

  }



  onForgotPasswordClick() {
    this.state = AuthenticatorCompState.FORGOT_PASSWORD;

    this.reactionform = this.formbuilder.group({
      forgotresetpwdemail: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    })

  }

  onCreateAccountClick() {
    this.state = AuthenticatorCompState.REGISTER;

    this.reactionform = this.formbuilder.group({
      regname: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
      regemail: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      regpassword: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
      regconpassword: new FormControl('', Validators.compose([Validators.required]))
    },
      {
        validators: this.Pwdmustmatch('regpassword', 'regconpassword')
      }
    )
  }

  onLoginClick() {
    this.state = AuthenticatorCompState.LOGIN;

    this.reactionform = this.formbuilder.group({
      loginemail: new FormControl('', Validators.compose([Validators.required])),
      loginpassword: new FormControl('', Validators.compose([Validators.required])),

    })

  }





  /*STATES*/
  isLoginState() {
    return this.state == AuthenticatorCompState.LOGIN;
  }

  isRegisterState() {
    return this.state == AuthenticatorCompState.REGISTER;
  }

  isForgotPasswordState() {
    return this.state == AuthenticatorCompState.FORGOT_PASSWORD;
  }

  isResetPasswordState() {
    return this.state == AuthenticatorCompState.RESET_PASSWORD;
  }

  getStateText() {
    switch (this.state) {
      case AuthenticatorCompState.LOGIN:
        return "Login";
      case AuthenticatorCompState.REGISTER:
        return "Register";
      case AuthenticatorCompState.FORGOT_PASSWORD:
        return "Forgot Password";
      case AuthenticatorCompState.RESET_PASSWORD:
        return "Reset Password";
    }
  }
  /** Register Submit */
  onRegSubmit(inputs: any): void {

    this.regname = inputs.regname;
    this.regemail = inputs.regemail;
    this.regpassword = inputs.regpassword;

    this.authService.register(this.regname, this.regemail, this.regpassword).subscribe({
      next: data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      error: err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    });
  }

  /** Login Submit */

  onLoginSubmit(inputs: any): void {

    this.loginemail = inputs.loginemail;
    this.loginpassword = inputs.loginpassword;

    this.authService.login(this.loginemail, this.loginpassword).subscribe({
      next: data => {

        this.storageService.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.storageService.getUser().roles;

        //this.router.navigate(['/home/postfeed']);

        this.loginSheet.dismiss();

        this.router.navigate(['/home/postfeed'])
          .then(() => {
            this.reloadPage();
          });


      },
      error: err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    });


  }

  /** Forgot Submit */

  onForgotSubmit(inputs: any): void {

    this.forgotresetpwdemailSubmit = true;
    this.forgotresetpwdemail = inputs.forgotresetpwdemail;

    this.authService.forgotpassword(this.forgotresetpwdemail).subscribe({
      next: data => {
        this.forgotresetpwdemailSubmit = true;
      },
      error: err => {
        this.forgotresetpwdemailSubmit = true;
      }
    });
    this.reactionform.reset();
  }

  /** Reset Submit */
  onResetSubmit(inputs: any): void {

    this.resetpwdtoken = inputs.resetpwdtoken;
    this.resetpwdnewpwd = inputs.resetpwdnewpwd;

    this.authService.resetpassword(this.resetpwdtoken, this.resetpwdnewpwd).subscribe({

      error: data => {
        if (data.status == 200) {
          this.resetpwdtokenSubmit = true;
          this.isresetFail = false;
          //console.log(this.resetpwdtokenSubmit + ":PASS" + this.isresetFail);
          this.errorMessage = '';

        } else {
          this.resetpwdtokenSubmit = true;
          this.isresetFail = true;
         // console.log(this.resetpwdtokenSubmit + ":FAIL" + this.isresetFail);
          this.errorMessage = "Invalid token or token expired. Please request forgot password for the token again.";

        }
      }
    });

  }

  get f() {
    return this.reactionform.controls;
  }



  Pwdmustmatch(password: any, conpassword: any) {
    return (formGroup: FormGroup) => {

      const passwordcontrol = formGroup.controls[password];
      const conpasswordcontrol = formGroup.controls[conpassword];

      if (conpassword.errors && !conpasswordcontrol.errors['Pwdmustmatch']) {
        return;
      }

      if (passwordcontrol.value !== conpasswordcontrol.value) {
        conpasswordcontrol.setErrors({ Pwdmustmatch: true });
      } else {
        conpasswordcontrol.setErrors(null);
      }


    };
  }
  reloadPage(): void {
    window.location.reload();
  }
}

export enum AuthenticatorCompState {
  LOGIN,
  REGISTER,
  FORGOT_PASSWORD,
  RESET_PASSWORD
}
