package com.dxc.entity;

import javax.persistence.*;





@Entity
public class Roles   {

	public enum ERole{
		ROLE_ADMIN,
		ROLE_USER
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private ERole name;
		
	public Roles() {
		
	}
	
	public Roles(ERole name) {
		this.name=name;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public ERole getName() {
		return name;
	}
	
	public void setName(ERole name) {
		this.name=name;
	}
	
	
	
	
}
