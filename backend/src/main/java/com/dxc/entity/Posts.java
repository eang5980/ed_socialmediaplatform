package com.dxc.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;




@Entity
public class Posts extends Auditable<String> implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private int viewcount;
	private String contenttype;
	
	@Column(columnDefinition="varchar(2048)")
	private String link;
	
	private String caption;
	
//	@JsonManagedReference
//	@OneToOne(targetEntity=Files.class,cascade=CascadeType.ALL)  
//	@OnDelete(action = OnDeleteAction.CASCADE)
//	@JoinColumn(name = "filesid", referencedColumnName = "id")
	
	
	@JsonBackReference
	@OneToOne(mappedBy = "posts")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Files files;  
	
	 @ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(name = "userid")
	 @OnDelete(action = OnDeleteAction.CASCADE)
	 //@JsonIgnore
	 private Users user;
	
	
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getViewcount() {
		return viewcount;
	}
	public void setViewcount(int viewcount) {
		this.viewcount = viewcount;
	}
	public String getContenttype() {
		return contenttype;
	}
	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}

	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public Files getFiles() {
		return files;
	}
	public void setFiles(Files files) {
		this.files = files;
	}
	
	
	
}
