package com.dxc.entity;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "files")
public class Files extends Auditable<String> implements Serializable {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String filename;
	private String filetype;
	

	@JsonManagedReference
	@OneToOne(targetEntity=Posts.class,cascade=CascadeType.REMOVE) //CascadeType.ALL)  
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "postsid", referencedColumnName = "id")
    private Posts posts;

	public Posts getPosts() {
		return posts;
	}

	public void setPosts(Posts posts) {
		this.posts = posts;
	}

	@Lob
	private byte[] data;
	
//	@OneToOne(targetEntity=Users.class,cascade = CascadeType.ALL)  
//	@JoinColumn(name = "id", referencedColumnName = "id")
//	private Posts post;  

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Files() {
		super();
	}

	public Files(String filename, String filetype, byte[] data) {
		super();
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
	}

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

//	public Posts getPost() {
//		return post;
//	}
//
//	public void setPost(Posts post) {
//		this.post = post;
//	}


	
}