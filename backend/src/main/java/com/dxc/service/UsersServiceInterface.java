package com.dxc.service;

import com.dxc.payload.UsersDTO;

public interface UsersServiceInterface {

	void deleteUserbyId(long id );
	
	UsersDTO updateUsers(UsersDTO usersDto, long id);
	
}
