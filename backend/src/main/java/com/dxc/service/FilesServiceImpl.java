package com.dxc.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.dxc.entity.Files;
import com.dxc.entity.Posts;
import com.dxc.exception.FileStorageException;
import com.dxc.exception.ResourceNotFoundException;
import com.dxc.payload.FilesDTO;
import com.dxc.repository.FilesRepository;



@Service
public class FilesServiceImpl implements FilesServiceInterface {

	@Autowired
	private FilesRepository filesRespository;
	
	
	
	
	//convert entity to DTO
	private FilesDTO mapToDto(Files files) {
		FilesDTO filesDto = new FilesDTO();
		filesDto.setId(files.getId());
		filesDto.setFilename(files.getFilename());
		filesDto.setFiletype(files.getFiletype());
		filesDto.setData(files.getData());
		filesDto.setPosts(files.getPosts());
		
		filesDto.setCreatedBy(files.getCreatedBy());
		filesDto.setCreatedDate(files.getCreatedDate());
		filesDto.setLastModifiedBy(files.getLastModifiedBy());
		filesDto.setLastModifiedDate(files.getLastModifiedDate());
		
		
		return filesDto;
	}
	
	//convert DTO to entity
	public Files mapToEntity(FilesDTO filesDTO) {
		Files files = new Files();
		files.setFilename(filesDTO.getFilename());
		files.setFiletype(filesDTO.getFiletype());
		files.setData(filesDTO.getData());
		files.setPosts(filesDTO.getPosts());
		
		
		files.setCreatedBy(filesDTO.getCreatedBy());
		files.setCreatedDate(filesDTO.getCreatedDate());
		files.setLastModifiedBy(filesDTO.getLastModifiedBy());
		files.setLastModifiedDate(filesDTO.getLastModifiedDate());
		
		
		return files;
	}
	

	
	@Override
	public FilesDTO storeFile(MultipartFile file) {
		String fileName=StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if(fileName.contains("..")) {
				throw new FileStorageException(
						"Sorry! FileName contains invalid path Sequence"+fileName
						);
			}
			Files files=new Files(fileName,file.getContentType(),file.getBytes());
			
		    return mapToDto(filesRespository.save(files));
		}catch(IOException e) {
			throw new FileStorageException("Could not Store File"+fileName+"please try again",e);
		}
		
	}
	
	@Override
	public FilesDTO getFile(String fileId) {
		
		Files findbyIdFiles = filesRespository.findById(fileId)
				.orElseThrow(
						()->new com.dxc.exception.FileNotFoundException(
								"File Not found with ID"+fileId
								)
						);
		return mapToDto(findbyIdFiles);
	}

	@Override
	public FilesDTO updateFile(FilesDTO filesDto, String fileId) {
		Files files = filesRespository.findById(fileId).orElseThrow(
				()->new com.dxc.exception.FileNotFoundException(
						"File Not found with ID"+fileId
						)
				);
	
//		if(postsDto.getViewcount() != 0)
//			posts.setViewcount(postsDto.getViewcount());      
//		if(postsDto.getContenttype()!=null)
//			posts.setContenttype(postsDto.getContenttype());			
//		if(postsDto.getLink() != null)
//			posts.setLink(postsDto.getLink());					
//		if(postsDto.getCaption()!=null)
//			posts.setCaption(postsDto.getCaption());						
//		if(postsDto.getId()!=null)
//			posts.setId(postsDto.getId());			
//		if(postsDto.getUser()!=null)
//			posts.setUser(postsDto.getUser());			
//		if(postsDto.getFiles()!=null)
//			posts.setFiles(postsDto.getFiles());	
		
		
		if(filesDto.getData()!=null)
			files.setData(filesDto.getData());			
		if(filesDto.getFilename()!=null)
			files.setFilename(filesDto.getFilename());			
		if(filesDto.getFiletype()!=null)
			files.setFiletype(filesDto.getFiletype());				
		if(filesDto.getId()!=null)
			files.setId(filesDto.getId());			
		if(filesDto.getPosts()!=null)
			files.setPosts(filesDto.getPosts());
		
		if(filesDto.getCreatedBy()!=null)
			files.setCreatedBy(filesDto.getCreatedBy());
		if(filesDto.getCreatedDate()!=null)
			files.setCreatedDate(filesDto.getCreatedDate());
		if(filesDto.getLastModifiedBy()!=null)
			files.setLastModifiedBy(filesDto.getLastModifiedBy());
		if(filesDto.getLastModifiedDate()!=null)
			files.setLastModifiedDate(filesDto.getLastModifiedDate());
		
		
		
		Files updateFiles = filesRespository.save(files);
		
		return mapToDto(updateFiles);
		
	}
	
}
