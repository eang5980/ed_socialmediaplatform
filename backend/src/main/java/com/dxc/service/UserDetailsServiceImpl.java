package com.dxc.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.dxc.entity.Users;
import com.dxc.exception.ResourceNotFoundException;
import com.dxc.payload.UsersDTO;
import com.dxc.repository.UsersRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, UsersServiceInterface {

	//3 hours
	private static final long EXPIRE_TOKEN_AFTER_MINUTES = 180;
	
	@Autowired
	UsersRepository usersRepository;
	
	// convert entity to DTO
	private UsersDTO mapToDto(Users users) {
		UsersDTO usersDto = new UsersDTO();
		usersDto.setId(users.getId());
		usersDto.setName(users.getName());
		usersDto.setPassword(users.getPassword());
		usersDto.setEmail(users.getEmail());
		usersDto.setRoles(users.getRoles());
		usersDto.setPosts(users.getPosts());
		
		
		usersDto.setCreatedBy(users.getCreatedBy());
		usersDto.setCreatedDate(users.getCreatedDate());
		usersDto.setLastModifiedBy(users.getLastModifiedBy());
		usersDto.setLastModifiedDate(users.getLastModifiedDate());

		return usersDto;
	}

	// convert DTO to entity
	public Users mapToEntity(UsersDTO usersDTO) {
		Users users = new Users();
		users.setName(usersDTO.getName());
		users.setPassword(usersDTO.getPassword());
		users.setEmail(usersDTO.getEmail());
		users.setRoles(usersDTO.getRoles());
		users.setPosts(usersDTO.getPosts());
		
		users.setCreatedBy(usersDTO.getCreatedBy());
		users.setCreatedDate(usersDTO.getCreatedDate());
		users.setLastModifiedBy(usersDTO.getLastModifiedBy());
		users.setLastModifiedDate(usersDTO.getLastModifiedDate());

		return users;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String email) {

		Users user = usersRepository.findByEmail(email);
		
		if (user == null) {
			throw new UsernameNotFoundException("User Not found!");
		}
		return UsersDetailsImpl.build(user);
	}

	@Override
	public void deleteUserbyId(long id) {
		// TODO Auto-generated method stub

		Users user = usersRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("DELETE", "id", id));

		usersRepository.delete(user);
	}

	@Override
	public UsersDTO updateUsers(UsersDTO usersDto, long id) {
		Users users = usersRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("POST", "id", id));

		

        if (usersDto.getName() != null)
        	users.setName(usersDto.getName());
        if (usersDto.getPassword() != null)
        	users.setPassword(usersDto.getPassword());
        if (usersDto.getEmail() != null)
        	users.setEmail(usersDto.getEmail());
        if (usersDto.getRoles().isEmpty() == false)
        	users.setRoles(usersDto.getRoles());
       if (  !CollectionUtils.isEmpty(usersDto.getPosts()))
        	users.setPosts(usersDto.getPosts());
        if (usersDto.getId()!=null)
        	users.setId(usersDto.getId());
        
        
        if(usersDto.getCreatedBy()!=null)
        	users.setCreatedBy(usersDto.getCreatedBy());
        if(usersDto.getCreatedDate()!=null)
        	users.setCreatedDate(usersDto.getCreatedDate());
        if(usersDto.getLastModifiedBy()!=null)
        	users.setLastModifiedBy(usersDto.getLastModifiedBy());
        if(usersDto.getLastModifiedDate()!=null)
        	users.setLastModifiedDate(usersDto.getLastModifiedDate());
//		
//		users.setName(usersDto.getName());
//		users.setPassword(usersDto.getPassword());
//		users.setEmail(usersDto.getEmail());
//		users.setRoles(usersDto.getRoles());
//		users.setPosts(usersDto.getPosts());

		Users updateUsers = usersRepository.save(users);
		return mapToDto(updateUsers);
	}

	public String forgotPassword(String email) {

		Optional<Users> userOptional = Optional
				.ofNullable(usersRepository.findByEmail(email));

		if (!userOptional.isPresent()) {
			return "Invalid email id.";
		}

		Users user = userOptional.get();
		user.setToken(generateToken());
		user.setTokenCreationDate(LocalDateTime.now());

		user = usersRepository.save(user);

		return user.getToken();
	}

	public String resetPassword(String resetpwdtoken, String password) {

		Optional<Users> userOptional = Optional
				.ofNullable(usersRepository.findByToken(resetpwdtoken));

		if (!userOptional.isPresent()) {
			return "Invalid token.";
		}

		LocalDateTime tokenCreationDate = userOptional.get().getTokenCreationDate();

		if (isTokenExpired(tokenCreationDate)) {
			return "Token expired.";

		}

		Users user = userOptional.get();

		user.setPassword(password);
		user.setToken(null);
		user.setTokenCreationDate(null);

		usersRepository.save(user);

		return "Your password successfully updated.";
	}

	//Generate unique token. 
	private String generateToken() {
		StringBuilder resetpwdtoken = new StringBuilder();

		return resetpwdtoken.append(UUID.randomUUID().toString())
				.append(UUID.randomUUID().toString()).toString();
	}

	// Check whether the created token expired or not.
	private boolean isTokenExpired(final LocalDateTime tokenCreationDate) {

		LocalDateTime now = LocalDateTime.now();
		Duration diff = Duration.between(tokenCreationDate, now);

		return diff.toMinutes() >= EXPIRE_TOKEN_AFTER_MINUTES;
	}
}
	

	

