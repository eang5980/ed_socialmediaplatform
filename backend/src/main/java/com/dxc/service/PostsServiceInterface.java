package com.dxc.service;

import com.dxc.payload.PostsDTO;
import com.dxc.payload.PostsResponse;

public interface PostsServiceInterface {
	PostsDTO createPost(PostsDTO postsDto);
	
	PostsResponse getAllPosts(int pagNo,int pageSize,String sortBy,String sortDir);
	
	PostsDTO getPostById(long id);
	
	PostsDTO updatePost(PostsDTO postsDto,long id );
	
	void deletePostById(long id);
	
}
