package com.dxc.service;


import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.dxc.entity.Posts;
import com.dxc.exception.ResourceNotFoundException;
import com.dxc.payload.PostsDTO;
import com.dxc.payload.PostsResponse;
import com.dxc.repository.PostsRepository;
import com.dxc.repository.UsersRepository;

@Service
public class PostsServiceImpl implements PostsServiceInterface {

	@Autowired
	private PostsRepository postsRepository;
	
	@Autowired
	private UsersRepository usersRepository;
	//convert entity to DTO
	private PostsDTO mapToDto(Posts posts) {
		PostsDTO postsDto = new PostsDTO();
		postsDto.setId(posts.getId());
		postsDto.setViewcount(posts.getViewcount());
		postsDto.setContenttype(posts.getContenttype());
		postsDto.setLink(posts.getLink());
		postsDto.setCaption(posts.getCaption());
		postsDto.setFiles(posts.getFiles());
		postsDto.setUser(posts.getUser());
		
		postsDto.setCreatedBy(posts.getCreatedBy());
		postsDto.setCreatedDate(posts.getCreatedDate());
		postsDto.setLastModifiedBy(posts.getLastModifiedBy());
		postsDto.setLastModifiedDate(posts.getLastModifiedDate());

		
		
		return postsDto;
	}
	
	//convert DTO to entity
	public Posts mapToEntity(PostsDTO postsDTO) {
		Posts posts = new Posts();
		posts.setViewcount(postsDTO.getViewcount());
		posts.setContenttype(postsDTO.getContenttype());
		posts.setLink(postsDTO.getLink());
		posts.setCaption(postsDTO.getCaption());
		posts.setFiles(postsDTO.getFiles());
		posts.setUser(postsDTO.getUser());
		
		posts.setCreatedBy(postsDTO.getCreatedBy());
		posts.setCreatedDate(postsDTO.getCreatedDate());
		posts.setLastModifiedBy(postsDTO.getLastModifiedBy());
		posts.setLastModifiedDate(postsDTO.getLastModifiedDate());
		
		return posts;
	}
	
	
	@Override
	public PostsDTO createPost(PostsDTO postsDto) {
		// convert Dto to entity
		Posts posts = mapToEntity(postsDto);
		
		Posts newPosts = postsRepository.save(posts);
		
		//convert entity to DTO
		PostsDTO postsResponse = mapToDto(newPosts);
		return postsResponse;
	}

	@Override
	public PostsResponse getAllPosts(int pagNo, int pageSize, String sortBy, String sortDir) {
		Sort sort= sortDir.equalsIgnoreCase(Sort.Direction.ASC.name())? 
				Sort.by(sortBy).ascending()
				: Sort.by(sortBy).descending();
			
		Pageable pagable = PageRequest.of(pagNo, pageSize,sort);
		Page<Posts> posts= postsRepository.findAll(pagable);
		
		//Get content from page object
		List<Posts> listofPosts = posts.getContent();
		List<PostsDTO> content= listofPosts.stream().map(
				post -> mapToDto(post)).collect(Collectors.toList()
				);
		
		PostsResponse postsResponse = new PostsResponse();
		postsResponse.setContent(content);
		postsResponse.setPageNo(posts.getNumber());
		postsResponse.setPageSize(posts.getSize());
		postsResponse.setTotalElement(posts.getTotalElements());
		postsResponse.setTotalPages(posts.getTotalPages());
		postsResponse.setLast(posts.isLast());
		// TODO Auto-generated method stub
		return postsResponse;
	}

	@Override
	public PostsDTO getPostById(long id) {
		Posts posts = postsRepository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("POST", "id", id)
				);
		return mapToDto(posts);
	}

	@Override
	public PostsDTO updatePost(PostsDTO postsDto, long id) {
		Posts posts = postsRepository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("POST", "id", id)
				);
		
		
//        if (usersDto.getName() != null)
//        	users.setName(usersDto.getName());
//        if (usersDto.getPassword() != null)
//        	users.setPassword(usersDto.getPassword());
//        if (usersDto.getEmail() != null)
//        	users.setEmail(usersDto.getEmail());
//        if (usersDto.getRoles() != null)
//        	users.setRoles(usersDto.getRoles());
//        if (usersDto.getPosts() != null)
//        	users.setPosts(usersDto.getPosts());
//		
		
		if(postsDto.getViewcount() != 0)
			posts.setViewcount(postsDto.getViewcount());      
		if(postsDto.getContenttype()!=null)
			posts.setContenttype(postsDto.getContenttype());			
		if(postsDto.getLink() != null)
			posts.setLink(postsDto.getLink());					
		if(postsDto.getCaption()!=null)
			posts.setCaption(postsDto.getCaption());						
		if(postsDto.getId()!=null)
			posts.setId(postsDto.getId());			
		if(postsDto.getUser()!=null)
			posts.setUser(postsDto.getUser());			
		if(postsDto.getFiles()!=null)
			posts.setFiles(postsDto.getFiles());
		
		
		if(postsDto.getCreatedBy()!=null)
			posts.setCreatedBy(postsDto.getCreatedBy());
		if(postsDto.getCreatedDate()!=null)
			posts.setCreatedDate(postsDto.getCreatedDate());
		if(postsDto.getLastModifiedBy()!=null)
			posts.setLastModifiedBy(postsDto.getLastModifiedBy());
		if(postsDto.getLastModifiedDate()!=null)
			posts.setLastModifiedDate(postsDto.getLastModifiedDate());
		
		

		Posts updatePosts = postsRepository.save(posts);
		return mapToDto(updatePosts);
	}
	
	
	@Override
	public void deletePostById(long id) {
		// TODO Auto-generated method stub
		Posts posts = postsRepository.findById(id).orElseThrow(
				()->new ResourceNotFoundException("DELETE", "id", id)
				);
		System.out.println("DELETE POST");
		postsRepository.delete(posts);
	}

}
