package com.dxc.service;

import com.dxc.entity.EmailDetails;

public interface EmailServiceInterface {

	String sendSimpleMail(EmailDetails details);

}
