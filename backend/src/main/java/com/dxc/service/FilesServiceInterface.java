package com.dxc.service;


import org.springframework.web.multipart.MultipartFile;


import com.dxc.payload.FilesDTO;
import com.dxc.payload.PostsDTO;

public interface FilesServiceInterface {

	FilesDTO storeFile(MultipartFile file);
	
	FilesDTO getFile(String fileId);
	
	FilesDTO updateFile(FilesDTO filesDto,String id );
	
	
}
