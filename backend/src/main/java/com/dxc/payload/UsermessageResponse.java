package com.dxc.payload;

public class UsermessageResponse {
	private String message;

	public UsermessageResponse(String message) {
	    this.message = message;
	  }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
