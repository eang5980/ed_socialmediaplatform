package com.dxc.payload;

import java.util.List;

public class UsersInfoResponse {
	private Long id;
	private String email;
	private String name;
	private List<String> roles;

	public UsersInfoResponse(Long id, String email, String name,List<String> roles) {
		this.id = id;
		this.email = email;
		this.roles = roles;
		this.name=name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<String> getRoles() {
		return roles;
	}
}
