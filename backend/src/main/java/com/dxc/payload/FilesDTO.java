package com.dxc.payload;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.dxc.entity.Posts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilesDTO {
	
	private String id;
	private String filename;
	private String filetype;
	private byte[] data;
	private Posts posts;

	  
	    private String createdBy;
	    private Date createdDate;
	    private String lastModifiedBy;
	    private Date lastModifiedDate;
	
	
}
