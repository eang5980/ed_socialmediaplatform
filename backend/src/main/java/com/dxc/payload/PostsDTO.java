package com.dxc.payload;


import java.util.Date;

import com.dxc.entity.Files;
import com.dxc.entity.Users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostsDTO {
	
	private Long id;
	private int viewcount;
	private String contenttype;
	private String link;
	private String caption;
	private Files files; 
	private Users user;
	
	
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
	
	
}
