package com.dxc.payload;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.dxc.entity.Posts;
import com.dxc.entity.Roles;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor

public class UsersDTO {

	private Long id;
	private String name;
	private String password;
	private String email;
	private Set<Roles> roles = new HashSet<>();
	private List<Posts> posts;
	
	
	
	
    private String createdBy;
    private Date createdDate;
    private String lastModifiedBy;
    private Date lastModifiedDate;
}
