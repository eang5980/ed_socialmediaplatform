package com.dxc.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilesResponse {
	
	private String fileName;
	private String fileDownload;
	private String fileType;
	private long size;
	private String fileId;
	

}
