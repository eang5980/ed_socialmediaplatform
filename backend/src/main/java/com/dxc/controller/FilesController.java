package com.dxc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.dxc.entity.Posts;
import com.dxc.entity.Roles.ERole;
import com.dxc.payload.FilesDTO;
import com.dxc.payload.FilesResponse;
import com.dxc.payload.PostsDTO;
import com.dxc.repository.UsersRepository;
import com.dxc.service.FilesServiceImpl;
import com.dxc.service.PostsServiceImpl;

@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600, allowCredentials="true")
@RestController
@RequestMapping("api/files")
//@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
public class FilesController {
	
	@Autowired
	private FilesServiceImpl fileStorageService;
	
	@Autowired
	private PostsServiceImpl postsServiceImpl;
	
	@Autowired
	UsersRepository usersRepository;
	
	@PostMapping("/uploadfile")
	public FilesResponse uploadFile(@RequestParam("file")MultipartFile file) {
		FilesDTO fileName=fileStorageService.storeFile(file);
		
		String fileDownloadUri=ServletUriComponentsBuilder.fromCurrentContextPath()
				               .path("/api/files/downloadfile/")
				               .path(fileName.getId())
				               .toUriString();
		return new FilesResponse(fileName.getFilename(), fileDownloadUri,file.getContentType(),file.getSize(),fileName.getId());
	}
	
	
	@GetMapping("/downloadfile/{fileId:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileId,HttpServletRequest request){
		
		FilesDTO files = fileStorageService.getFile(fileId);
		
		
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(files.getFiletype()))
				.header(HttpHeaders.CONTENT_DISPOSITION,"attachment:filename=\""+files.getFilename()+"\"")
                .body(new ByteArrayResource(files.getData()));
	}
	
	
		//Update file
		@PutMapping("/{id:.+}/{postid}")
		public ResponseEntity<String> updateFile(@PathVariable String id,@PathVariable long postid){
//			
			
			FilesDTO filesDto = new FilesDTO();
			
			PostsDTO postsDto = postsServiceImpl.getPostById(postid);
			Posts posts = postsServiceImpl.mapToEntity(postsDto);
			posts.setId(postid);
			filesDto.setId(id);
			filesDto.setPosts(posts);
			
			fileStorageService.updateFile(filesDto, id);

			return new ResponseEntity<String>("Update Sucessfully", HttpStatus.OK);
			
		}
	
}
