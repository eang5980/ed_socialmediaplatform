package com.dxc.controller;

import javax.validation.Valid;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.security.access.prepost.PreAuthorize;

import com.dxc.entity.EmailDetails;
import com.dxc.entity.Roles;
import com.dxc.entity.Roles.ERole;
import com.dxc.entity.Users;
import com.dxc.exception.ResourceNotFoundException;
import com.dxc.payload.LoginRequest;
import com.dxc.payload.SignupRequest;
import com.dxc.payload.UsermessageResponse;
import com.dxc.payload.UsersDTO;
import com.dxc.payload.UsersInfoResponse;
import com.dxc.repository.RolesRepository;
import com.dxc.repository.UsersRepository;
import com.dxc.service.EmailServiceImpl;
import com.dxc.service.UserDetailsServiceImpl;
import com.dxc.service.UsersDetailsImpl;
import com.dxc.utils.JwtUtils;

@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600, allowCredentials="true")
@RestController
@RequestMapping("/api/users")

public class UsersController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UsersRepository userRepository;

	@Autowired
	RolesRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;
	

	
	@Autowired 
	private EmailServiceImpl emailService;

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		UsersDetailsImpl userDetails = (UsersDetailsImpl) authentication.getPrincipal();

		ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
				.body(new UsersInfoResponse(userDetails.getId(), userDetails.getEmail(), userDetails.getName(), roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new UsermessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		Users user = new Users(signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()),
				signUpRequest.getName());

		Set<String> strRoles = signUpRequest.getRole();
		Set<Roles> roles = new HashSet<>();

		if (strRoles == null) {
			Roles userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Roles adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);
					break;
				default:
					Roles userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new UsermessageResponse("User registered successfully!"));
	}

	@PostMapping("/signout")
	public ResponseEntity<?> logoutUser() {
		ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
		return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
				.body(new UsermessageResponse("You've been signed out!"));
	}

	/* START Only role admin has access */

	// Get all Post REST API
	@GetMapping("/getallusers")
	@PreAuthorize("hasRole('ADMIN')")
	public List<Users> getAllUsers() {
		return userRepository.findAll();
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<String> deleteUserById(@PathVariable(name = "id") long id) {

		userDetailsServiceImpl.deleteUserbyId(id);
		return new ResponseEntity<String>("User Entity deleted successfully", HttpStatus.OK);
	}

	/* END Only role admin has access */


	// Update user by ID
	@PutMapping("update/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<String> updateUser(@RequestBody UsersDTO usersDto, @PathVariable(name = "id") long id) {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		boolean isAdminRole = authentication.getAuthorities().stream()
				.anyMatch(r -> r.getAuthority().equals(ERole.ROLE_ADMIN.toString()));

		if (isAdminRole) {
			if (usersDto.getPassword() != null) {
				usersDto.setPassword(encoder.encode(usersDto.getPassword()));
			}
			userDetailsServiceImpl.updateUsers(usersDto, id);
			return new ResponseEntity<String>("User Updated Successfully", HttpStatus.OK);
		} else {
			if (userRepository.findByEmail(authentication.getName()).getId() == id) {
				if (usersDto.getPassword() != null) {
					usersDto.setPassword(encoder.encode(usersDto.getPassword()));
				}
				userDetailsServiceImpl.updateUsers(usersDto, id);
				return new ResponseEntity<String>("User Updated Successfully", HttpStatus.OK);
			}
		}
		return new ResponseEntity<String>("Update fail, not authorized", HttpStatus.FORBIDDEN);

	}
	
	// Get user by ID
		
		@SuppressWarnings("unchecked")
		@GetMapping("/{id}")
		@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
		public ResponseEntity<Optional<Users>> getUser(@PathVariable(name = "id") long id) {

			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			boolean isAdminRole = authentication.getAuthorities().stream()
					.anyMatch(r -> r.getAuthority().equals(ERole.ROLE_ADMIN.toString()));

			if (isAdminRole) {
				Optional<Users> user =userRepository.findById(id);
				if(user.isPresent() == false) {
					return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
				}
				System.out.println(user);
				return ResponseEntity.ok(user);
			} else {
				if (userRepository.findByEmail(authentication.getName()).getId() == id) {
					Optional<Users> user =userRepository.findById(id);
					if(user.isPresent() == false) {
						return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
					}
					return ResponseEntity.ok(user);		
					
				}
			}
			return (ResponseEntity<Optional<Users>>) ResponseEntity.badRequest();

		}
	
	
	@PostMapping("/forgotpassword")
	public ResponseEntity<String> forgotPassword(@RequestParam String email) {
	
		String response = userDetailsServiceImpl.forgotPassword(email);

		if (!response.startsWith("Invalid")) {
		//	response = "http://localhost:8080/api/users/resetpassword?token=" + response;
			response = "Your reset password token is : " + response + "\n Please enter this token in our resetpassword page. Thanks! \n The token will expire in 3hours ";
			
			EmailDetails details = new EmailDetails(email, response, "ED-SocialMediaPlatform Reset Password");
			emailService.sendSimpleMail(details);
			return new ResponseEntity<String>("email sent Successfully", HttpStatus.OK);
			
		}
		return new ResponseEntity<String>("email sent fail", HttpStatus.FORBIDDEN);
	}

	@PutMapping("/resetpassword")
	public ResponseEntity<String> resetPassword(@RequestParam String token,
			@RequestParam String password) {
		
	
		password = encoder.encode(password);
		String response = userDetailsServiceImpl.resetPassword(token, password);
		if (!response.startsWith("Invalid")) {
		return new ResponseEntity<String>("reset password success", HttpStatus.OK);
		}
		return new ResponseEntity<String>("reset password fail", HttpStatus.FORBIDDEN);
	}
	
//	@PostMapping("/sendmail")
//    public String
//    sendMail(@RequestBody EmailDetails details)
//    {
//        String status
//            = emailService.sendSimpleMail(details);
// 
//        return status;
//    }

	
}
