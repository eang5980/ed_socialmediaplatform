package com.dxc.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxc.entity.Roles.ERole;
import com.dxc.payload.PostsDTO;
import com.dxc.payload.PostsResponse;
import com.dxc.repository.FilesRepository;
import com.dxc.repository.RolesRepository;
import com.dxc.repository.UsersRepository;
import com.dxc.service.PostsServiceInterface;
import com.dxc.utils.PostsConst;

@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600, allowCredentials="true")
@RestController
@RequestMapping("api/posts")
@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
public class PostsController {

	@Autowired
	private PostsServiceInterface postsService;
	
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UsersRepository userRepository;

	@Autowired
	RolesRepository roleRepository;
	
	@Autowired
	FilesRepository fileRepository;
	
	//create post
	//@SuppressWarnings("deprecation")
	@PostMapping("/{email}")
	public ResponseEntity<PostsDTO> createPost(@RequestBody PostsDTO postsDto ,@PathVariable(name="email")String email){
		
		postsDto.setUser(userRepository.findByEmail(email));
		return new ResponseEntity<>(postsService.createPost(postsDto), HttpStatus.CREATED);
	}

	
	//Get all Post REST API
	@GetMapping
	public PostsResponse getAllPosts(
			@RequestParam(value="pageNo",defaultValue = PostsConst.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
			@RequestParam(value="pageSize", defaultValue = PostsConst.DEFAULT_PAGE_SIZE,required=false) int pageSize,
			@RequestParam(value="sortBy",defaultValue = PostsConst.DEFAULT_SORT_BY,required=false) String sortBy,
			@RequestParam(value="sortDir", defaultValue = PostsConst.DEFAULT_SORT_DIRECTION,required=false) String sortDir
			) {
				return postsService.getAllPosts(pageNo, pageSize, sortBy, sortDir);
		
	}
	//Get all Post by Id
	@GetMapping("/{id}")
	public ResponseEntity<PostsDTO> getPostById(@PathVariable(name="id")long id){
		return ResponseEntity.ok(postsService.getPostById(id));
	}
	//Update post by ID
	@PutMapping("/{id}")
	public ResponseEntity<PostsDTO> updatePost(@RequestBody PostsDTO postsDto,@PathVariable(name="id") long id){
		

		PostsDTO postsResponse = new PostsDTO();
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		boolean isAdminRole = authentication.getAuthorities().stream()
				.anyMatch(r -> r.getAuthority().equals(ERole.ROLE_ADMIN.toString()));
	
		if (isAdminRole) {
			postsResponse = postsService.updatePost(postsDto, id);
			return new ResponseEntity<PostsDTO>(postsResponse,HttpStatus.OK);
		} else {
			postsResponse = postsService.updatePost(postsDto, id);
			return new ResponseEntity<PostsDTO>(postsResponse,HttpStatus.OK);
//			if (userRepository.findByEmail(authentication.getName()).getId() == id) {
//				postsResponse = postsService.updatePost(postsDto, id);
//				return new ResponseEntity<PostsDTO>(postsResponse,HttpStatus.OK);
//			}
		}
		
			//return new ResponseEntity<PostsDTO>(postsResponse,HttpStatus.FORBIDDEN);
		
	}
	
	//Delete post by Id
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deletePostById(@PathVariable (name="id") long id){
		
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		boolean isAdminRole = authentication.getAuthorities().stream()
				.anyMatch(r -> r.getAuthority().equals(ERole.ROLE_ADMIN.toString()));
	
		if (isAdminRole) {
			postsService.deletePostById(id);
			return new ResponseEntity<String>("Post Entity deleted successfully",HttpStatus.OK);
		} else {
			postsService.deletePostById(id);
			return new ResponseEntity<String>("Post Entity deleted successfully",HttpStatus.OK);
//			System.out.println(authentication.getName() + "TEST");
//			if (userRepository.findByEmail(authentication.getName()).getId() == id) {
//				postsService.deletePostById(id);
//				return new ResponseEntity<String>("Post Entity deleted successfully",HttpStatus.OK);
//			}
		
//		return new ResponseEntity<String>("Deletion Error",HttpStatus.FORBIDDEN);
	}
	}
}

