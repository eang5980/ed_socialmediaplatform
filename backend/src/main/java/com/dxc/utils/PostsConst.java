package com.dxc.utils;

public class PostsConst {
	public static final String DEFAULT_PAGE_NUMBER = "0";
	public static final String DEFAULT_PAGE_SIZE = "2147483647";
	public static final String DEFAULT_SORT_BY="createdDate";
	public static final String DEFAULT_SORT_DIRECTION="desc";
	
}
