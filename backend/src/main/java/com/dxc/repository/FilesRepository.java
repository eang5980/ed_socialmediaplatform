package com.dxc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.entity.Files;

@Repository
public interface FilesRepository extends JpaRepository<Files, String> {

}
