package com.dxc.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.entity.Roles;
import com.dxc.entity.Roles.ERole;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Long> {

	Optional<Roles> findByName(ERole name);
	
}
