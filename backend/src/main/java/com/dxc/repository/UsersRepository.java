package com.dxc.repository;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.entity.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

	  Users findByEmail(String email);

	  Boolean existsByEmail(String email);
	  
	  Users findByToken(String resetpwdtoken);
	

	
	  

	
}
