# ED Social Media Platform Using Java Spring Boot & Angular

## Overview
Social media has become a popular platform for sharing and creation of content. The aim is to create your own platform using Java, Spring and Angular.

## Assumptions
-   A user can be both role admin and role user
-   No 2 users of the same role can have the same email address
-   Password need to be encrypted and then stored into the database 
-   Depending on content type, one of the field is mandatory . <br>
        - contenttype is link , link field will be mandatory.   
        - contenttype is mediaupload, media field will be mandatory.
-   File multi-upload is not allowed. Each post only allow 1 file to be uploaded
## Technology Stack/ Software Used


### <ins> Backend: Spring Tool Suite v4.12.1   </ins>
  -  Java Spring Boot v2.7.3
  -  Java 8
  -  JDK 1.8

### <ins> Frontend: Visual Studio Code v1.71.0  </ins>
  -  Angular 14.2.1

### <ins> Database  </ins>
  -  MySQL 8.0.30
 
<br>

## High Level Design
![SocialMediaPlatform_HighLevelDesign.png](./docs/SocialMediaPlatform_HighLevelDesign.png)

### <ins> Frontend </ins>
- Admin Login
    - Admin Webpage
        - View all users details
            - Update user's name
            - Delete user account
        - Post feed able to delete/update all post

    - Includes users' feature
- User Login
    - User Webpage (10 posts each time, able to view more)
        - View posts
            - Update post created by user
            - Delete post created by user

##### Sample Screens 
![SampleScreens.PNG](./docs/SampleScreens.PNG)

## Requirements
1. There should be Feeds page to show the post contents
    - For simplicity, all users will see the same posts
    - The page should be paginated showing 10 posts each time
2. Each post should be able to cater for either a hyperlink or media (video) and a caption
    - Clicking on the post will open the link or play the video
3. Users are required to login to view and post
    - There should be a sign-up page for accounts creation
4. Users should be able to create a post that appears on the Feeds
5. When creating a post, users should be able to upload files or provide a link to the media
    - Local filesystem storage be used
6. A post can only be updated by the user who created it
7. There should be an Admin role that can manage the platform
    - Maintain posts (Delete/update any posts)
    - Maintain users (Delete/update)
8. The system should be able to track the number of views for each post
    - Each click on the post is considered 1 view



<ins> Technical considerations </ins>

Entities should be well-designed and include audit fields such as created by and created date
Refer to Spring documentation https://docs.spring.io/spring-data/jpa/docs/1.7.0.DATAJPA-580-SNAPSHOT/reference/html/auditing.html

APIs should be secured as much as possible. Minimally only logged in user can access.
There should be validations for inputs and messages should be displayed
The design for Angular should be in small reusable components to cater for the requirements

Use different components to display the different media types


Consider using 3 types of post to cater for hyperlink, image and video

The fields, link and media, are optional depending on the post type


Stored medias should be deleted also when deleting a post


<ins> Additional Implementation </ins>

- Developing SMTP protocol to deliver the Email 
    - when click on forgot password, user will receive the mail notification to reset the password.

- Encryption and Decryption business logic to be implemented on password creation to avoid security breach.

